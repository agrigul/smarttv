﻿/*
 *  Works with state of the application (history)
*/
enyo.kind({
    name: "ApplicationStateService",
    //* @public
    
    /*
     *  Save the state object to the history of browser. State object should contain 
     *  name of controller , name  of action, and params object (if required)
    */
    SaveState: function (stateObject) {
        this._checkStateObject(stateObject);
        this._SaveCurrentStateToHistory(stateObject);
    },
    
    /*
     *  Returns the top(last avaliable) state from history stack
     */
    GetCurrentState: function () {
        return history.state;
    },
    
    
    /*
     * Replaces the last history state with new value
     */
    UpdateCurrentState: function (stateObject) {
        this._checkStateObject(stateObject);
        history.replaceState(stateObject);
    },
    

    //* @private
    _SaveCurrentStateToHistory: function (stateObject) {
        if (stateObject) {
            history.pushState(stateObject);
        }
    },
    
    _checkStateObject: function (stateObject) {
        if (!stateObject) {
            throw Error("State object can't be null");
        }
        if (!stateObject.controller || stateObject.controller === "") {
            throw Error("Controller property in state object can't be null");
        }
        
        if (!stateObject.action || stateObject.action === "") {
            throw Error("Action property in state object can't be null");
        }
    }
});

﻿/*
 * Router controls the page navigation and history update for propper work of the Back button
*/
enyo.kind({
    name: "RouterService",
    kind: "enyo.Object",
    _appStateService: new ApplicationStateService(),
    
    //* @public
    
    /*
     *  Opens the page according to its name and arguments
     */
    RouteTo: function (page, args, context) {
        
        if (!page || typeof page !== "string")
            throw Error("Page should be specified");
        
        var pageDelegate;
        switch (page) {
            case pages.Programs:
                pageDelegate = this._ToPrograms;
                break;
                
            case pages.Details:
                pageDelegate = this._ToDetails;
                break;
                
            case pages.ProgramWatcher:
                pageDelegate = this._ToProgramWatcher;
                break;

            case pages.Channels:
                pageDelegate = this._ToChannels;
                break;
                
            default: console.log("Page" + page + "doesn't exist in routes");
        }
        
        this._SaveLastSelectedChannel(context); //Fix: requred for correct Back button handling, after several channels were selected and user goes returns from watching page back to channels
        
        if (pageDelegate) {
            pageDelegate.apply(this, args);
        }
    },
    
    
    //* @private
    
    
    /*
     * while your select different channels on channels view, you have to save your last selected channel in state
     */
    _SaveLastSelectedChannel: function (context) {
        
        if (context.$.baseLayoutView.$.renderSection.$.ChannelsView) {
            
            var currentState = this._appStateService.GetCurrentState();
            
            if (currentState) {
                var currentChannelId = context.$.baseLayoutView.$.renderSection.$.ChannelsView.getCurrentChannelId();
                
                if (currentChannelId && currentState.params.channelId != currentChannelId) {
                    currentState.params.channelId = currentChannelId;
                    this._appStateService.UpdateCurrentState(currentState);
                }
            }
        }
    },
    
    _ToPrograms: function () {
        
        var stateObject = {
            controller: "programsController", 
            action: "Index", 
            params: null
        };
        this._appStateService.SaveState(stateObject);
        
        return app.$.programsController.Index();
    },
    
    _ToDetails: function () {
        
        var programId = arguments[1].id;
        var stateObject = {
            controller: "detailsController", 
            action: "Index", 
            params: { id: programId }
        }
        this._appStateService.SaveState(stateObject);
        
        return app.$.detailsController.Index({ id: programId });
    },
    
    _ToProgramWatcher : function () {
        
        var channelId = arguments[1].channelId;
        var stateObject = {
            controller: "programWatcherController", 
            action: "Index", 
            params: { channelId: channelId }
        }
        this._appStateService.SaveState(stateObject);
        
        return app.$.programWatcherController.Index({ channelId: channelId });
    },
    
    _ToChannels: function () {
        
        var channelId = null;
        if (arguments[1] && arguments[1].channelId)
            channelId = arguments[1].channelId;
        
        var stateObject = {
            controller: "channelsController", 
            action: "Index", 
            params: { channelId: channelId }
        };
        this._appStateService.SaveState(stateObject);
        
        return app.$.channelsController.Index({ channelId: channelId });
    },
});



/*
 *  Global enum with names of pages for Router service
 */
window.pages = {
    Programs : "Programs",
    Details : "Details",
    ProgramWatcher : "ProgramWatcher",
    Channels : "Channels"
}

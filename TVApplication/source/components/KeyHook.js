enyo.kind({
    name: "KeyHook",
    kind:"enyo.Component",
    mixins:[enyo.RegisteredEventSupport],
    create: enyo.inherit(function(sup){
        return function(){
            sup.apply(this, arguments);
            var keyHookModel = new KeyHookModel({
                id: this.get("buttonId")
            });
            this.set("model", keyHookModel );
            keyHookModel.fetch();
        };
    }),
    published:{
        buttonId: "",
        buttonName: "",
        buttonCode: 0,
        defaultAction: function(){
            console.log("defaultAction");
        }
    },
    events: {
        actionFired: "",
        keyHookDown: "",
        keyHookUp: ""
    },
    components:[
        { kind: "enyo.Signals", onkeypress: "_fireAction", onkeyup: "keyHookUp", onkeydown:"keyHookDown" }
    ],
    _defaultActionEnabled: true,
    _fireAction: function(sender, event){
        if(event.keyCode != this.buttonCode) return;
        if(this._defaultActionEnabled) this.defaultAction(this);
        this.triggerEvent("actionFired", this);
    },
    keyHookDown: function () {
        if(event.keyCode != this.buttonCode) return;
        if(this._noninitial) return;
        this.triggerEvent("keyHookDown");
        this._noninitial = true;
    },
    keyHookUp: function(){
        if(event.keyCode != this.buttonCode) return;
        this._noninitial = false;
        this.triggerEvent("keyHookUp");
    },
    enableDefaultAction: function(){
        this._defaultActionEnabled = true;
    },
    disableDefaultAction: function(){
        this._defaultActionEnabled = false;
    }
});
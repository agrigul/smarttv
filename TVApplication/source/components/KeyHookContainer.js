(function(enyo, window){
    window.KeyHooksEnum = {
        Button1: { buttonName: "1", buttonCode: 49 },
        Button2: { buttonName: "2", buttonCode: 50 },
        Button3: { buttonName: "3", buttonCode: 51 },
        Button4: { buttonName: "4", buttonCode: 52 },
        Button5: { buttonName: "5", buttonCode: 53 },
        Button6: { buttonName: "6", buttonCode: 54 }
    };

    enyo.kind({
        name: "KeyHookContainer",
        kind: "enyo.Component",
        create: enyo.inherit(function(sup){
            return function(){
                sup.apply(this, arguments);
                var KeyHooks = window.KeyHooksEnum;
                for( var buttonId in KeyHooksEnum){
                    this._hooks.push( new KeyHook({
                        buttonId: buttonId,
                        buttonName: KeyHooksEnum[buttonId].buttonName,
                        buttonCode: KeyHooksEnum[buttonId].buttonCode,
                        defaultAction: this.get("defaultHookAction")
                    }));
                }
            };
        }),
        published:{
            defaultHookAction: function(){
                console.log("default from manager");
            }
        },
        _hooks: [],
        getHooks: function(){
            return this._hooks;
        },
        enableDefaultAction: function(){
            enyo.forEach(this._hooks, function(hook){
                hook.enableDefaultAction();
            });
        },
        disableDefaultAction: function(){
            enyo.forEach(this._hooks, function(hook){
                hook.disableDefaultAction();
            });
        }

    });
})(enyo, window);
enyo.depends(
	// Cordova (PhoneGap) library
	//"$lib/enyo-cordova",

	// webOS-specific kind library
	//"$lib/enyo-webos",

	// Internationalization library
	//"$lib/enyo-ilib",
    
	// Layout library
    "$lib/layout",

	// Moonstone UI library
	"$lib/moonstone",

	// Focus support library for TV applications
	"$lib/spotlight",

    // Include global config file
    "config.js",

	// custom components
	"components",
    
	// CSS/LESS style files
	"style",
	
    // services which handles data and makes operations
    "services",

    // repositories which load data from server
    "repositories",
    
    // Model and data definitions
	"models",

    // controllers 
    "controllers",

	//common controls defenitions
	"controls",

	// View kind definitions
	"views",
	
	// Include our default entry point
	"app.js"

);

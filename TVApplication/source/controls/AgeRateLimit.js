﻿/*
 * Image of the age rate limit
 */
enyo.kind({
    name: "AgeRateLimit",
    classes: "vertical-center circle",
    style: "display: none",
    published: {
        ageRateValue: 0
    },
    
    /*
     * Property sets the icon with age rate limit (18+)
     */
    ageRateValueChanged: function (sender, newValue) {
        if (newValue) {
            this.set("content", "+" + newValue);
            this.addStyles("display:;");
        }
        if (!newValue || newValue < 1 || newValue == '') {
            this.addStyles("display: none;");
        }
    }
});
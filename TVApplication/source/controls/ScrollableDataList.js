enyo.kind({
    name: "ScrollableDataList",
    kind: "moon.DataList",
    handlers: {
        onSpotlightFocused: "itemFocused"
    },
    renderDelay: null,
    itemFocused: function(){
        var focused = this.getFocusedChild();
        if(focused) this.$.scroller.scrollToControl(focused, false, true);
    }
});
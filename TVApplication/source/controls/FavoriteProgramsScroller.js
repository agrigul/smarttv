﻿/*
 * Scroller which displays  icons of programs, title and start, end time
 * */
enyo.kind({
    name: 'FavoriteProgramsScroller',
    kind: 'moon.Scroller',
    vertical: "hidden",
    spotlight: "container",
    style: "white-space: nowrap;",
    
    published: {
        collection: null,
        selectedItemId: null, // the model.id of an item which was focused and could be opened
    },
    
    collectionChanged: function (empty, value) {
        
        if (value && value.kindName === "enyo.Collection") {
            return value;
        }
        else if (Array.isArray(value)) {
            return new enyo.Collection(value);
        } 
        else {
            return new enyo.Collection([value]);
        }
    },
    
    bindings: [
        { from: ".collection", to: ".$.itemsRepeater.collection" },
        { from: ".selectedItemProgramId", to: ".$.itemsRepeater.selectedItemProgramId", oneWay: false }
    ],
    
    components: [
        {
            kind: "ItemsRepeater"
        }
    ]
});


enyo.kind({
    name: "ItemsRepeater",
    kind: "enyo.DataRepeater",
    selectedItemId : null,
    selectedChannelId: null,
    components: [
        {
            kind: "ItemWithProgram"
        }
    ],
    
    handlers: {
        onSpotlightFocused: 'onSpotlightFocusedHandler'
    },
    
    /*
     *  Set's the program id for selected item
    */
    onSpotlightFocusedHandler: function (oSender, oEvent) {
        
        if (!oEvent || !oEvent.model) {
            throw Error("favorite program model  can't be null or undefined");
        }
        else if (!oEvent.model.get("id")) {
            throw Error("favorite program id can't be null or undefined");
        }
        
        this.set("selectedItemProgramId", oEvent.model.get("id"));
        this.set("selectedChannelId", oEvent.model.get("channelId"));
        
    },
    
    select : function () {
        var channelId = this.get("selectedChannelId");
        app.OpenPage(pages.ProgramWatcher, { channelId: channelId });
    }
});


/*
 *  Template for Scroller item
 */
enyo.kind({
    name: "ItemWithProgram",
    kind: "moon.Item",
    style: "display:inline-block;",
    classes: "moon-scroller-sample-item enyo programs-favorite-item",
    model: null,
    
    
    bindings: [
        { from: ".model.image_url", to: ".$.image.src" },
        { from: ".model.title", to: ".$.title.content" },
        { from: ".model.channel_name", to: ".$.channelName.content" },

        {
            from: ".model.start_time", to: ".$.programProgress.startTime", transform: function (timeStamp) {
                return new Date(timeStamp * 1000);
            
            }
        },
        {
            from: ".model.end_time", to: ".$.programProgress.endTime", transform: function (timeStamp) {
                return new Date(timeStamp * 1000);
            
            }
        }
    ],
    
    components: [
        {
            classes: "programs-crop-container programs-favorite-item-imageblock",
            components: [
                {
                    name: "image",
                    kind: "enyo.Image"
                }
            ]
        },
        {
            classes: "programs-item-tine-line-box",
            components: [
                {
                    name: "channelName",
                    classes: "programs-fonts-scroller-channel-name",
                },
                {
                    name: "title",
                    classes: "programs-fonts-scroller-item-name",
                    content: "название не указано"
                },
                {
                    kind: "ProgramProgress"
                }
            ]
        }
    ]
});
(function(){
    var PROGRESS_INTERVAL = 1; //update progressbar every minute

    enyo.kind({
        name: "ProgramProgress",
        classes: "programProgress",
        create: enyo.inherit(function(sup){
            return function(){
                sup.apply(this, arguments);
                this.makeProgress();
            };
        }),
        published: {
            startTime: new Date(),
            endTime: new Date((new Date()).getTime() + 60000)
        },
        computed: {
            startTimeLabel: ["startTime"],
            endTimeLabel: ["endTime"]
        },
        bindings:[
            {from: ".startTimeLabel", to: ".$.startTimeLabel.content"},
            {from: ".endTimeLabel",   to: ".$.endTimeLabel.content"}
        ],
        observers: {
            makeProgress: ["startTime", "endTime"]
        },
        /* computeds definitions start ================================= */
        startTimeLabel: function () {
            var startTime = this.get("startTime");
            return this.formatTime(startTime.getHours(), startTime.getMinutes());
        },
        endTimeLabel: function () {
            var endTime = this.get("endTime");
            return this.formatTime(endTime.getHours(), endTime.getMinutes());
        },
        /* computeds definitions end =================================== */
        makeProgress: function () {
            var startTime = this.get("startTime"),
                endTime = this.get("endTime"),
                currentTime = new Date();
            if(!(startTime instanceof Date) || !(endTime instanceof Date)){
                var errorMessage = "ProgramProgress: program start/end are not dates";
                console.error(errorMessage +  " Start: " + startTime + " End: " + endTime);
                return;
            }
            if(startTime > endTime){ //start date after end date
                this.$.progressBar.set("progress", 0);
                //console.error("ProgramProgress: start time is after end time");
                return;
            }
            if(currentTime > endTime) { // program has finished
                this.$.progressBar.set("progress", 100);
                return;
            }
            if(currentTime < startTime){ //program hasn`t yet started
                var startProgressingAfter = (startTime.getTime() - currentTime.getTime()) / 1000; //seconds
                this.$.progressBar.set("progress", 0);
                this.planProgress(startProgressingAfter);
                return;
            }
            var intervalLength = endTime.getTime() - startTime.getTime(),
                programProgress = currentTime.getTime() - startTime.getTime();
            this.$.progressBar.set("progress", programProgress * 100 / intervalLength);
            this.planProgress(PROGRESS_INTERVAL);
        },
        planProgress: function(timeout){
            this.startJob("programProgress", enyo.bind(this, function(){
                this.makeProgress();
            }), timeout);
        },
        formatTime: function (hours, minutes) {
            return hours + ":" + (minutes < 10 ? "0" : "") + minutes;
        },
        components:[
            {
                classes: "timeContainer",
                components:[
                    {name: "startTimeLabel", content: "", classes: "startTime"},
                    {name: "endTimeLabel", content: "", classes: "endTime"}
                ]
            },
            {
                name: "progressBar",
                kind: "moon.ProgressBar",
                classes: "progressBar",
                progress: 0
            }
        ]
    });

})();
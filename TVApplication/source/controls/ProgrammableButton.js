/**
 * @enum ProgrammableButtonType - enum for defining programmableButton instances.
 */
window.ProgrammableButtonType = {
    /**
     * @property {number} keyCode - keyboard shrotcut code for programmable RCU button
     * @property {string} cssClass - css class added to button. This class adds colored rectangle to button
     */
    Red: { keyCode: 403, cssClass: "programmableButton__red", toString: function () { return "Red"; }, valueOf: function () { return "Red"; } },
    Green: { keyCode: 404, cssClass: "programmableButton__green", toString: function () { return "Green"; }, valueOf: function () { return "Green"; } },
    Yellow: { keyCode: 405, cssClass: "programmableButton__yellow", toString: function () { return "Yellow"; }, valueOf: function () { return "Yellow"; } },
    Blue: { keyCode: 406, cssClass: "programmableButton__blue", toString: function () { return "Blue"; }, valueOf: function () { return "Blue"; } }
};

/**
 * @class ProgrammableButton - control that is abstraction of RCU programmable button (red/green/yellow/blue).
 * Could be pressed via click or keyboard shortcut (in TV RCU is actually a keyboard). For each Programmable button
 * instance, button type(@enum ProgrammableButtonType) should be provided via buttonType property.
 *
 */
enyo.kind({
    name: "ProgrammableButton",
    kind: "enyo.Button",
    
    create: enyo.inherit(function (sup) {
        return function () {
            sup.apply(this, arguments);
            
            var buttonType = ProgrammableButtonType[this.buttonType] || ProgrammableButtonType.Red;
            this.addClass(buttonType.cssClass);
            this.buttonType = buttonType;
        };
    }),

    published: {
        /**
         * spotlight-like border around button
         */
        isActive: false,
        /**
         * programmable button type (red/green/yellow/blue). Default is Red
         */
        buttonType: ProgrammableButtonType.Red
    },
    events: {
        /**
         * fired when buton is clicked or keyboard shortcut for button is pressed
         */
        onPress: ""
    },

    handlers: {
        onmousedown	: 'Press',
        onmouseup	: 'Undepress'
    },

    isActiveChanged: function () {
        this.addRemoveClass("active", this.get("isActive"));
    },

    Press: function () {
        this.addClass('pressed');
        this.doPress();
    },

    Undepress: function () {
        this.removeClass('pressed');
    },

    KeyClick: function (sender, event) {
        if (event.keyCode === this.buttonType.keyCode) {
            this.Press();
            setTimeout(enyo.bind(this, function () {
                    this.Undepress();
                }) , 200);
        }
    }
});

/*
 * Controller manages the current program stream from tv and ProgramWatcherView view
*/
enyo.kind({
    name : "ProgramWatcherController",
    kind: "enyo.Controller",
    create: enyo.inherit(function(sup){
        return function(){
            sup.apply(this, arguments);
            this.set("model", new InitialChannelModel());
            this.model.fetch();
        };
    }),

    Index : function(params) {
        params = params || {};
        var schedule = new ScheduleCollection(),
            channelId = params.channelId || this.getDefaultChannel();
        if(channelId != undefined){
            schedule.set("channelId", channelId);
            schedule.fetch({
                success: enyo.bind(this, this.setDefaultChannel)
            });
        }
        else{
            this.fetchDefaultChannel();
        }
        this.owner.UpdateView("ProgramWatcherView", schedule);
    },

    /**
     * @private
     */
    getDefaultChannel: function(){
        return this.model.get("channelId");
    },

    /**
     * @private
     */
    setDefaultChannel: function(collection, strategy, result){
        this.model.set("channelId", result.data.channel.id);
        this.model.set("channelTitle", result.data.channel.title);
        this.model.set("stream_url", result.data.channel.stream_url);
        this.model.commit();
    },

    /**
     * @private
     */
    fetchDefaultChannel: function(){
        var channelsList = new ChannelsCollection();
        channelsList.fetch({
            success: enyo.bind(this, function(collection){
                var defaultChannelId = collection.records[0].id;
                this.Index({channelId: defaultChannelId});
            })
        });
    }
});
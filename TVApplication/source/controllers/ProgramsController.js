﻿/*
 * Controller manages the favorite programs view
*/
enyo.kind({
    name : "ProgramsController",
    kind: "enyo.Controller",
    
    Index : function () {
        // logic ...
        var programsRepository = new ProgramsRepository();
        var res = programsRepository.GetFavoritePrograms();
        this.owner.UpdateView("ProgramsView", res);
    }

});
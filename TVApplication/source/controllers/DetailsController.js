﻿/*
 * Controller manages the details of the program
*/
enyo.kind({
    name : "DetailsController",
    kind: "enyo.Controller",
    
    Index : function (params) {
        var id = params.id;
        // logic ...

        var programsRepository = new ProgramsRepository();
        this.owner.UpdateView("DetailsView", programsRepository.GetProgramDetailsById(id));
    },

});
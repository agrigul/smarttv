﻿/*
 * Controller manages a list of channels
*/
enyo.kind({
    name : "ChannelsController",
    kind: "enyo.Controller",
    
    Index : function (params) {
        // logic ...
        var channelId = params.channelId;
        var channelsCollection = new ChannelsCollection(),
            scheduleCollection = new ScheduleCollection(),
            model = new enyo.Model({
                channelsCollection: channelsCollection,
                schedule: scheduleCollection,
                currentChannelId : channelId
            });
        this.owner.UpdateView("ChannelsView", model);
    }
});
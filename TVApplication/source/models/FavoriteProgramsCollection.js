﻿/*
 * Collection of FavoriteProgramsItemModel  items 
 * */
enyo.kind({
    name: "FavoriteProgramsCollection",
    kind: "enyo.Collection",
    defaultSource: "cdntvServer",
    url: "categories",

    parse: function (result) {
        return result.data.suited_programs;
    }
});
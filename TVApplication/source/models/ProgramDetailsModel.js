﻿/*
 * Model of details of program
 */
enyo.kind({
    name: "ProgramDetailsModel",
    kind: "enyo.Model",
    defaultSource: "cdntvServer",
    getUrl: function(){
//        return "programs/5";
        //TODO to integrate this model with production code uncomment code below
        return ["programs/", this.get("id")].join("");
    },
    primary_key: "id",
    defaults:{
        image_url: "",
        stream_url: "",
        start_time: new Date(),
        end_time: new Date( (new Date()).getTime() + 60000 )
    },
    
    /*
     * maps DTO objects from server to client Model
     */
    parse: function (result) {
        
        if (result && result.data) {
            this.set('id', result.data.id);
            this.set('title', result.data.title);
            this.set('rate', result.data.rate);
            this.set('image_url', result.data.image_url);
            this.set('stream_url', result.data.stream_url);
            this.set('start_time', new Date(result.data.start_time * 1000));
            this.set('end_time', new Date(result.data.end_time * 1000));
            this.set('short_description', result.data.short_description); // TODO: ask where to display this content (done. No response yet)
            this.set('description', result.data.description);
            
            this.set('logo_channel_url', result.data.logo_channel_url);
            this.set('logo_film_type_url', result.data.logo_film_type_url);
            this.set('channel_name', result.data.channel_name);
            this.set('original_title', result.data.original_title);
            this.set('film_type', result.data.film_type);
            this.set('year', result.data.year);
            this.set('genre', result.data.genre);

        }
        
        return result;
    },

    didFetch: function () {
        this.inherited(arguments);
    }
});

enyo.kind({
    name: "ScheduleProgramModel",
    kind: "enyo.Model",
    defaults: {
        isCurrent: false,
        isPast: false
    },
    _timeoutId: null,
    constructor: enyo.inherit(function(sup){
        return function(){
            sup.apply(this, arguments);
            var startTime = this.get("start_time"),
                endTime = this.get("end_time"),
                currentTime = new Date();
            if(currentTime > endTime){
                this.set("isPast", true);
            }
            if(currentTime > startTime && currentTime < endTime){
                this.set("isCurrent", true);
            }
            if(currentTime < startTime){
                var startProgramAfter = startTime.getTime() - currentTime.getTime();
                this._timeoutId = setTimeout(enyo.bind(this, function(){
                    this.set("isCurrent", true);
                }), startProgramAfter);
            }
        };
    }),
    observers:{
        isCurrentChanged: ["isCurrent"]
    },
    isCurrentChanged: function(){
        var isCurrent = this.get("isCurrent");
        if(isCurrent){
            var finishAfter = this.get("end_time").getTime() - (new Date()).getTime();
            this._timeoutId = setTimeout(enyo.bind(this, function(){
                this.set("isCurrent", false);
                this.set("isPast", true);
            }), finishAfter);
        }
    },
    didDestroy: enyo.inherit(function(sup){
        return function(){
            clearTimeout(this._timeoutId);
            sup.apply(arguments);
        };
    })
});
enyo.kind({
    name: "ChannelsCollection",
    kind: "enyo.Collection",
    defaultSource: "cdntvServer",
    url: "channels",
    
    parse: function (result) {
        return result.data.all_channels;
    },
    
    getChannelById: function (id) {
        if (this.records) {
            for (var i = 0; i < this.records.length; i++) {
                if (this.records[i].get("id") &&
                    this.records[i].get("id") === id) {
                    return this.records[i];
                }
            }
        } else return null;
    }
});

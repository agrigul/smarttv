enyo.depends(
    "FavoriteProgramsCollection.js",
    "ProgramDetailsModel.js",
    "InitialChannelModel.js",
    "ChannelsCollection.js",
    "ScheduleProgramModel.js",
    "ScheduleCollection.js",
    "KeyHookModel.js"
);

/**
 * Channel that should be opened on application start;
 */
enyo.kind({
    name: "InitialChannelModel",
    kind: "enyo.Model",
    defaultSource: "customLocalStorage",
    primaryKey: "id",
    defaults: {
        id: "defaultChannelModel", // id to store model in local storage, not a channel id
        channelId: undefined,
        channelTitle: "",
        stream_url: ""
    }
});
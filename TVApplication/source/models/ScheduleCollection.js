enyo.kind({
    name: "ScheduleCollection",
    kind: "enyo.Collection",
    published: {
        channelId: undefined,
        channelTitle: "",
        stream_url: ""
    },
    model: "ScheduleProgramModel",
    defaultSource: "cdntvServer",
    getUrl: function () {
        //return "channelschedule";
        //TODO to integrate this collection with production code uncomment code below
        var channelId = this.channelId;
        if (channelId == undefined) throw new Error("ScheduleCollection:please provide channelId to fetch collection");
        return ["channels/", channelId, "/schedule"].join("");
    },
    parse: function (result) {
        var schedule = enyo.map(result.data.schedule, function (program) {
            return {
                id: program.id,
                title: program.title,
                ageRate: program.ageRate,
                start_time: new Date(program.start_time * 1000),
                end_time: new Date(program.end_time * 1000),
                short_description: program.short_description
            };
        });
        schedule = this.removeExpiredPrograms(schedule);
        return schedule;
    },
    didFetch: enyo.inherit(function (sup) {
        return function (record, options, response) {
            this.set("stream_url", response.data.channel.stream_url);
            this.set("channelTitle", response.data.channel.title);
            sup.apply(this, arguments);
        };
    }),
    getCurrentProgram: function () {
        var currentProgram = this.filter(function (program) {
            return program.get("isCurrent");
        });
        return currentProgram[0];
    },
    
    removeExpiredPrograms: function (schedule) {
        var currentTime = new Date();
        for (var i = 0; i < schedule.length; i++) {
            if (schedule[i].end_time < currentTime) {
                schedule.splice(i, 1);
            }
        }
        
        return schedule;
    }
});
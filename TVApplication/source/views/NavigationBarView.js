﻿/*
 *  View for main Navigation bar
 * */
enyo.kind({
    name: "NavigationBarView",
    layoutKind: "FittableColumnsLayout",
    classes: "fittable-box programs-navigation-bar", 
    components: [
        {
            kind: "NavigationButton", 
            content: 'Каналы'
        },
        {
            kind: "NavigationButton", 
            content: 'Передачи',
            classes: "button-active"
        },
        { fit: true }
    ]
});

/*
 * Button for navigation bar
 * */
enyo.kind({
    name: "NavigationButton",
    tag: "span",
    kind: "moon.Button",
    classes: "navigation-bar-button",
});
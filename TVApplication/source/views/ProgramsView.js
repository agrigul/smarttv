﻿/*
 * View  for programs page
 */
enyo.kind({
    name: "ProgramsView",
    kind: "enyo.View",
    layoutKind: "FittableRowsLayout",
    classes: "enyo-fit programs-main-view",
    model: null,
    bindings: [
        {
            from: ".model", to: ".$.favoriteProgramsView.$.favoriteProgramsScroller.collection"
        }
    ],
    
    components: [
        //{ kind: "NavigationBarView" }, //NOTE: navigation bar was removed, but may be required in future. May be deleted after 20.03.2015
        { kind: "FavoriteProgramsView", fit: true },
        { kind: "ColoredButtonsNavigationMenuInPrograms" }
    ]
});




/*
 * View with content
 * */
enyo.kind({
    name: "FavoriteProgramsView",
    kind: "FittableColumns",
    classes: "lg-screen-padding",
    components: [
        {
            kind: "FittableRows",
            fit: true,
            components: [
                {
                    name: "TitleForCarousel",
                    classes: "fittable-box programs-maint-title",
                    content: "Сейчас вы можете посмотреть:",
                },
                {
                    components: [
                        { kind: "FavoriteProgramsScroller", fit: true }
                    ]
                }
            ]
        }
    ],
    
    GetFocusedProgramId : function () {
        return this.$.favoriteProgramsScroller.selectedItemProgramId;
    }
});


/*
 *  Footer with navigation buttons
*/
enyo.kind(
    {
        name: "ColoredButtonsNavigationMenuInPrograms",
        classes: "footerMenu lg-screen-padding-navbar",
        components: [
            {
                name: "RedButton",
                kind: "ProgrammableButton",
                buttonType: ProgrammableButtonType.Red,
                classes: "stackLeft",
                content: "Каналы",
                onPress: "OpenChannelsHandler"
            },
            {
                name: "BlueButton",
                kind: "ProgrammableButton",
                buttonType: ProgrammableButtonType.Blue,
                classes: "stackRight",
                content: "Описание",
                onPress: "OpenDetailsHandler"
            },
            { kind: "enyo.Signals", onkeydown: "ColoredButtonClickHandler" }
        ],
        
        ColoredButtonClickHandler : function (sender, event) {
            switch (event.keyCode) {
                case ProgrammableButtonType.Red.keyCode:
                    this.$.RedButton.KeyClick(sender, event);
                    break;
                case ProgrammableButtonType.Blue.keyCode:
                    this.$.BlueButton.KeyClick(sender, event);
                    break;
                default: break;
            }
        },
        
        OpenChannelsHandler: function () {
            app.OpenPage(pages.Channels);
        },
        
        
        OpenDetailsHandler: function () {
            var id = this.owner.$.favoriteProgramsView.GetFocusedProgramId();
            if (id) {
                app.OpenPage(pages.Details, { id: id });
            }
        },
    });
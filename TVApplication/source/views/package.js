enyo.depends(
    // view of the Screen with details of the Program
    "BaseLayoutView.js",
    "DetailsView.js",
    "ChannelsView.js",
    "ProgramsView.js",
    "NavigationBarView.js",
    "ProgramWatcherView.js"
);

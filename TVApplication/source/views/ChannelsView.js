﻿enyo.kind({
    name: "ChannelsView",
    kind: "enyo.View",
    classes: "channelsView",
    layoutKind: "FittableRowsLayout",
    fit: true,
    bindings: [
        { from: ".model.channelsCollection", to: ".$.channelsList.collection" },
        { from: ".model.schedule", to: ".$.scheduleList.collection" },
        {
            from: ".currentChannel", to: ".$.scheduleList.currentChannel", transform: function (v) {
                return v;
            }
        }
    ],
    
    
    create: enyo.inherit(function (sup) {
        return function () {
            sup.apply(this, arguments);
            this.initHookButtons();
            this.model.attributes.channelsCollection.fetch({ success: enyo.bind(this, this.channelsReady) });
        };
    }),
    
    
    channelsReady: function () {
        var firstChannel;
        if (this.model.get("currentChannelId"))
            firstChannel = this.model.attributes.channelsCollection.getChannelById(this.model.get("currentChannelId"));
        else
            firstChannel = this.model.attributes.channelsCollection.at(0);
        if (firstChannel) this.selectChannel(firstChannel);
    },
    
    
    initHookButtons: function () {
        var hooks = app.getHooks(),
            favoriteButtonsContainer = this.$.favoriteButtonsContainer,
            bindChannelCallback = enyo.bind(this, this.bindChannel);
        enyo.forEach(hooks, function (hook) {
            favoriteButtonsContainer.createComponent({
                kind: "ChannelView.FavoriteChannelButton",
                hook: hook,
                action: bindChannelCallback
            });
        });
    },
    
    
    openFavoritePrograms: function () {
        app.OpenPage(pages.Programs);
    },
    
    
    channelClick: function (inSender, event) {
        var channelModel = inSender.model;
        this.selectChannel(channelModel);
    },
    
    
    getCurrentChannelId: function () {
        if (this.currentChannel && this.currentChannel.get("isSelected"))
            return this.currentChannel.get("id");
        else return null;
    },
    
    
    selectChannel: function (channelModel) {
        var previousChannel = this.get("currentChannel");
        if (previousChannel) { previousChannel.set("isSelected", false); }
        channelModel.set("isSelected", true);
        this.set("currentChannel", channelModel);
        var channelId = channelModel.get("id"),
            channelUrl = channelModel.get("stream_url");
        this.model.attributes.schedule.set("channelId", channelId);  // Fix. Set the channelId before load data
        this.model.attributes.schedule.fetch({
            destroyLocal: true,
            replace: true,
            success: enyo.bind(this, this.scheduleFetched)
        });
        this.$.videoPlayer.set("src", channelUrl);
    },
    
    
    scheduleFetched: function () {
        setTimeout(enyo.bind(this, function () {
                var schedulePrograms = this.$.scheduleList.getClientControls(),
                    focusIndex = enyo.find(schedulePrograms, function (program) {
                        return program.isCurrent == true;
                    });
                if (focusIndex !== false) {
                    enyo.Spotlight.spot(schedulePrograms[focusIndex]);
                }
                this.resized(); // fix: resize scroller to display arrows

            }, 100));
    },
    
    
    bindChannel: function (hook) {
        var currentChannel = this.get("currentChannel");
        hook.model.set({
            channelId: currentChannel.get("id"),
            channelTitle: currentChannel.get("title"),
            channelUrl: currentChannel.get("stream_url"),
            channelIcon: currentChannel.get("logo_url")
        });
        hook.model.commit();
    },
    
    
    components: [
        {
            name: "videoPlayer",
            kind: "enyo.Video",
            classes: "backgroundVideo",
            autoplay: true
        },
        {
            classes: "channelsHeader",
            components: [
                { classes: "channelsHeaderLabel stackLeft", content: "Нажмите кнопку 1-6 и у удерживайте в течении  3 секунд чтобы добавить телеканал в избранное" },
                { name: "favoriteButtonsContainer", classes: "stackRight" }
            ]
        },
        {
            fit: true,
            layoutKind: "FittableColumnsLayout",
            components: [
                {
                    name: "channelsList",
                    kind: "ScrollableDataList",
                    classes: "channelsListColumn",
                    components: [
                        {
                            kind: "ChannelView.ChannelItem",
                            ontap: "channelClick"
                        }
                    ]
                },
                {
                    classes: "programsList",
                    fit: true,
                    components: [
                        {
                            name: "scheduleList",
                            kind: "ScrollableDataList",
                            components: [
                                {
                                    kind: "ChannelView.ScheduleProgram"
                                }
                            ]
                        }
                    ]
                }
            ]
        },
        { kind: "ColoredButtonsNavigationMenuInChannels" }
    ]
});


/*
 *  Footer with navigation buttons
*/
enyo.kind(
    {
        name: "ColoredButtonsNavigationMenuInChannels",
        classes: "footerMenu",
        components: [
            {
                name: "GreenButton",
                kind: "ProgrammableButton",
                buttonType: ProgrammableButtonType.Green,
                classes: "stackLeft",
                content: "Передачи",
                onPress: "OpenFavoriteProgramsHandler"
            },
            { kind: "enyo.Signals", onkeydown: "ColoredButtonClickHandler" }
        ],
        
        ColoredButtonClickHandler : function (sender, event) {
            if (event.keyCode == ProgrammableButtonType.Green.keyCode) {
                this.$.GreenButton.KeyClick(sender, event);
            }
        },
        
        OpenFavoriteProgramsHandler: function () {
            app.OpenPage(pages.Programs);
        }
    });




enyo.kind({
    name: "ChannelView.ChannelItem",
    kind: "enyo.Control",
    classes: "listItem",
    spotlight: true,
    published: {
        isSelected: false
    },
    bindings: [
        { from: ".model.logo_url", to: ".$.channelIcon.src" },
        { from: ".model.title", to: ".$.channelTitle.content" },
        { from: ".model.isSelected", to: ".isSelected" }
    ],
    observers: {
        isSelectedChanged: ["isSelected"]
    },
    isSelectedChanged: function () {
        var isSelected = this.get("isSelected");
        this.addRemoveClass("channelSelected", isSelected);
    },
    components: [
        {
            classes: "imageCont",
            components: [
                { name: "channelIcon", kind: "enyo.Image" }
            ]
        },
        {
            name: "channelTitle", classes: "channelTitle"
        }
    ]
});

enyo.kind({
    name: "ChannelView.ScheduleProgram",
    kind: "enyo.Control",
    create: enyo.inherit(function (sup) {
        return function () {
            sup.apply(this, arguments);
            this.isCurrentChanged();
        };
    }),
    classes: "programItem",
    published: {
        isPast: false,
        isCurrent: false
    },
    bindings: [
        {
            from: ".model.start_time",
            to: ".$.programTime.content",
            transform: function (date) {
                return date.getHours() + ":" + (date.getMinutes() < 10 ? "0" : "") + date.getMinutes();
            }
        },
        { from: ".model.ageRate", to: ".$.ageRate.ageRateValue" },
        { from: ".model.title", to: ".$.programTitle.content" },
        { from: ".model.short_description", to: ".$.programDescription.content" },
        { from: ".model.start_time", to: ".$.programProgress.startTime" },
        { from: ".model.end_time", to: ".$.programProgress.endTime" },
        { from: ".model.isPast", to: ".isPast" },
        { from: ".isPast", to: ".showing", transform: function (isPast) { return !isPast; } },
        { from: ".model.isCurrent", to: ".isCurrent" }
    ],
    isPastChanged: function () {
        this.addRemoveClass("programItem__past", this.get("isPast"));
    },
    isCurrentChanged: function () {
        var isCurrent = this.get("isCurrent");
        this.$.progressBarContainer.setShowing(isCurrent);
        this.addRemoveClass("programItem__current", isCurrent);
    },
    openProgramDetails: function () {
        var programId = this.model.get("id");
        app.OpenPage(pages.Details, { id: programId });
    },
    watchProgram: function () {
        if (!this.model.get("isCurrent"))
            return;
        
        var channelId = this.owner.currentChannel.get("id"); // fix: the id should be from channel, not from program
        app.OpenPage(pages.ProgramWatcher, { channelId: channelId });
    },
    handlers: {
        onSpotlightFocused: "onSpotlightFocused",
        onSpotlightBlur: "onSpotlightBlur"
    },
    onSpotlightFocused: function () { this.addClass("selected") },
    onSpotlightBlur: function () { this.removeClass("selected") },
    components: [
        {
            name: "programHeader",
            classes: "programHeader",
            spotlight: true,
            ontap: "watchProgram",
            components: [
                { classes: "programTime", name: "programTime" },
                {
                    classes: "programTitleContainer",
                    components: [
                        { name: "programTitle", classes: "programTitle" },
                    ]
                },
                {
                    name: "progressBarContainer",
                    classes: "progressBarContainer",
                    components: [
                        { kind: "ProgramProgress", name: "programProgress" }
                    ]
                },
                { name: "ageRate", kind: "AgeRateLimit", classes: "channels-schedule-age-rate-mini" }
            ]
        },
        {
            classes: "programDetails",
            components: [
                {
                    classes: "programDescriptionContainer",
                    components: [
                        { classes: "programDescription", name: "programDescription" }
                    ]
                },
                {
                    name: "moreInfoButton",
                    kind: "moon.Button",
                    classes: "moreInfoButton",
                    spotlight: true,
                    ontap: "openProgramDetails"
                }
            ]
        }
    ]
});


enyo.kind({
    name: "ChannelView.FavoriteChannelButton",
    kind: "moon.Button",
    spotlight: false,
    published: {
        hook: {},
        action: function () {

        }
    },
    saveTimeout: 2000,
    classes: "favoriteChannelButton",
    create: enyo.inherit(function (sup) {
        return function () {
            sup.apply(this, arguments);
            var hook = this.get("hook");
            this.set("content", hook.buttonName);
            this._bindedPress = enyo.bind(this, this.press);
            hook.disableDefaultAction();
            this.press = enyo.bind(this, this.press);
            this.depress = enyo.bind(this, this.depress);
            hook.addListener("keyHookDown", this.press);
            hook.addListener("keyHookUp", this.depress);
        };
    }),
    destroy: enyo.inherit(function (sup) {
        return function () {
            sup.apply(this, arguments);
            var hook = this.get("hook");
            hook.enableDefaultAction();
            hook.removeListener("keyHookDown", this.press);
            hook.removeListener("keyHookUp", this.depress);
        };
    }),
    handlers: {
        onmousedown: "press",
        onmouseup: "depress",
        onanimationEnd: "blinkingEnd",
        onwebkitAnimationEnd: "blinkingEnd"
    },
    press: function () {
        this.addClass("pressed");
        this._startSaveTimeout();
    },
    depress: function () {
        this.removeClass("pressed");
        this._teardownActionTimeout();
    },
    _startSaveTimeout: function () {
        this._actionTimeoutId = setTimeout(enyo.bind(this, this.invokeAction), this.saveTimeout);
    },
    _teardownActionTimeout: function () {
        clearTimeout(this._actionTimeoutId);
    },
    invokeAction: function () {
        this.action(this.hook);
        this.addClass("blinking");
    },
    blinkingEnd: function () {
        this.removeClass("blinking");
    }
});

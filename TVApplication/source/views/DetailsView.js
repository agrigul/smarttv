var detailsMainView = "DetailsMainView";
var DetailsLeftPartOfScreenView = "detailsLeftColumnView";
var DetailsRightPartOfScreenView = "detailsRightColumnView";
var DetailsProgramInfoBox = "detailsProgramInfoBox";
var DetailsProgramImageBox = "detailsProgramImageBox";
var DetailsDescriptionScroller = "detailsDescriptionScroller";
var DetailsProgramDetailsHeader = "detailsProgramDetailsHeader";

var backButtonNavigation = "BackButtonNavigation";

/*
 *  View for details page
 */
enyo.kind({
    name: "DetailsView",
    kind: "enyo.View",
    layoutKind: "FittableRowsLayout",
    classes: "enyo-fit  fittable-box main-background",
    published: {
        model: null
    },
    
    bindings: [
        { from: ".model", to: ".$.detailsMainView.model" }
    ],
    components: [
        { kind: detailsMainView, fit: true }
    ],
    
    buttonTapped : function (sender, event) {
        app.OpenPage(pages.Programs);
    }
});

/*
 *  Class for button "Back" on Navigation bar
 */
enyo.kind({
    name: backButtonNavigation,
    kind: "moon.Button", 
    content: "Назад",
    classes: "back-button",
    ontap : "buttonTapped"
});


enyo.kind({
    name: detailsMainView,
    kind: enyo.Control,
    
    layoutKind: "FittableColumnsLayout",
    classes: "moon fittable-box details-main-view",
    style: "height:955px;", //TODO: height need to be fixed. enyo-fit css class problem
    
    components: [
        { kind: DetailsLeftPartOfScreenView }, // left column
        { kind: DetailsRightPartOfScreenView } // right column
    ],
    
    published: {
        model: null
    },
    
    events: {
        onModelLoadCompleted : ""
    },
    
    handlers: {
        onModelLoadCompleted : "modelLoadCompletedHandler"
    },
    
    modelLoadCompletedHandler : function () {
        setTimeout(enyo.bind(this, function () {  // Bigfix: is required for focused arrow on scroller after description from model had been loaded
                this.resized();
                enyo.Spotlight.spot(this.$.detailsRightColumnView.$.detailsDescriptionScroller.$.strategy.$.pageDownControl);
            }), 0);
        return true;
    },
    bindings: [
        { from : ".model.title", to: ".$." + DetailsLeftPartOfScreenView + ".$." + DetailsProgramInfoBox + ".$.programTitle.content" },
        { from : ".model.rate", to: ".$." + DetailsLeftPartOfScreenView + ".$." + DetailsProgramInfoBox + ".$.ageRate.ageRateValue" },
        { from : ".model.image_url", to: ".$." + DetailsLeftPartOfScreenView + ".$." + DetailsProgramImageBox + ".$.image.src" },
        { from : ".model.logo_channel_url", to: ".$." + DetailsLeftPartOfScreenView + ".$." + DetailsProgramInfoBox + ".$.channelLogoImg.src" },
        { from : ".model.logo_film_type_url", to: ".$." + DetailsLeftPartOfScreenView + ".$." + DetailsProgramInfoBox + ".$.filmTypeImg.src" },
        { from : ".model.channel_name", to: ".$." + DetailsLeftPartOfScreenView + ".$." + DetailsProgramInfoBox + ".$.channelName.content" },
        { from : ".model.film_type", to: ".$." + DetailsLeftPartOfScreenView + ".$." + DetailsProgramInfoBox + ".$.detailsFilmTypeName.content" },
        { from : ".model.title", to: ".$." + DetailsRightPartOfScreenView + ".$." + DetailsProgramDetailsHeader + ".$.programTitle.content" },
        { from : ".model.original_title", to: ".$." + DetailsRightPartOfScreenView + ".$." + DetailsProgramDetailsHeader + ".$.originalProgramTitle.content" },
        { from : ".model.year", to: ".$." + DetailsRightPartOfScreenView + ".$." + DetailsProgramDetailsHeader + ".$.year.content" },
        { from : ".model.genre", to: ".$." + DetailsRightPartOfScreenView + ".$." + DetailsProgramDetailsHeader + ".$.genre.content" },
        {
            from : ".model.description", to: ".$." + DetailsRightPartOfScreenView + ".$." + DetailsDescriptionScroller + ".$.description.content", 
            transform: function (description) {
                if (description) {
                    this.doModelLoadCompleted();
                }
                return description;
            }
        }
    ]
});






// -------------------------------------  Left part of the screen

/*
* Left column  with  title and image of program
*/
enyo.kind({
    name: DetailsLeftPartOfScreenView,
    kind: "FittableRows",
    classes: "fittable-box details-left-column-view",
    content: "Left panel",
    components: [
        { kind: DetailsProgramInfoBox }, 
        { kind: DetailsProgramImageBox },
    ]
});


/*
* Info  about the program
*/
enyo.kind({
    name : DetailsProgramInfoBox,
    classes: "fittable-box ",
    kind: "FittableRows",
    components: [

        {
            tag: "div",
            classes: "fittable-box details-info-box details-block-distance",
            components: [

                {
                    // logo of the channel
                    kind: "enyo.Image", 
                    name : "channelLogoImg",
                    //src: "assets/details_channel_logo.png", // TODO: get url from server
                    classes: "fittable-box vertical-center details-icon"
                },
                {
                    tag : "span",
                    name : "channelName",
                    content: "не указан",
                    classes: "fittable-box vertical-center details-fonts-program-title"
                }
            ]
        },
        {
            tag: "div",
            classes: "fittable-box details-info-box details-block-distance",
            components: [

                {
                    tag: "span",
                    name : "programTitle",
                    style: "width: 88%;",
                    classes: "fittable-box vertical-center details-fonts-main-title"
                },
                {
                    name: "ageRate", kind: "AgeRateLimit", classes: "details-info-box-ageRate"
                }
            ]
        },
        {
            tag: "div",
            classes: "fittable-box details-info-box details-block-distance",
            components: [

                {
                    kind: "enyo.Image", 
                    name : "filmTypeImg",
                    // src: "assets/icon-movie-tape.png", // TODO: get url from server
                    classes: "fittable-box vertical-center ",
                    style : "width: 35px; height: 35px;"
                },
                {
                    tag: "span",
                    name : "detailsFilmTypeName",
                    classes: "fittable-box vertical-center details-fonts-program-type",
                }
            ]
        }
    ]
});


/*
 * Image of the program
 */
enyo.kind({
    name: DetailsProgramImageBox,
    kind: "FittableRows",
    classes: "fittable-box",
    components: [
        {
            classes: "fittable-box details-crop-container details-imagebox details-block-distance",
            components: [
                { kind: "enyo.Image", classes: "" }
            ]
        }
    ]
});








// -------------------------------------  Right part of the screen

/*
 * Right column with description of program
 */
enyo.kind({
    name: DetailsRightPartOfScreenView,
    kind: "FittableRows",
    classes: "fittable-box details-right-column-view",
    content: "Right panel",
    components: [
        { kind: DetailsProgramDetailsHeader }, 
        { kind: DetailsDescriptionScroller }
    ]
});


/*
 *  Header for details of the program
 */
enyo.kind({
    name: DetailsProgramDetailsHeader,
    //tag: "div",
    kind: "FittableRows",
    content: "details header",
    classes: "fittable-box details-block-distance",
    components: [
        {
            tag: "div",
            classes: "fittable-box details-content-distance",
            components: [
                {
                    tag: "b",
                    classes: "fittable-box details-fonts-program-details-header",
                    content: "Русское название: "
                },
                {
                    tag: "span",
                    classes: "fittable-box details-fonts-program-details-header",
                    name : "programTitle",
                    //content: "Будущее с Джеймсом Вудсом"
                }
            ]
        },

        {
            tag: "div",
            classes: "fittable-box details-content-distance",
            components: [
                {
                    tag: "b",
                    classes: "fittable-box details-fonts-program-details-header",
                    content: "Оригинальное название: "
                },
                {
                    name: "originalProgramTitle",
                    tag: "span",
                    classes: "fittable-box details-fonts-program-details-header",
                }
            ]
        },

        {
            tag: "div",
            classes: "fittable-box details-content-distance",
            components: [
                {
                    tag: "b",
                    classes: "fittable-box details-fonts-program-details-header",
                    content: "Год выпуска: "
                },
                {
                    tag: "span",
                    name: "year",
                    classes: "fittable-box details-fonts-program-details-header",
                    content: "не указан"
                }
            ]
        },

        {
            tag: "div",
            classes: "fittable-box details-content-distance",
            components: [
                {
                    tag: "b",
                    classes: "fittable-box details-fonts-program-details-header",
                    content: "Жанр: "
                },
                {
                    tag: "span",
                    name: "genre",
                    classes: "fittable-box details-fonts-program-details-header",
                    content: "не указан"
                }
            ]
        },

        {
            tag: "div",
            classes: "fittable-box details-content-distance",
            components: [
                {
                    tag: "b",
                    classes: "fittable-box details-fonts-program-details-header",
                    content: "Выпущено: "
                },
                {
                    tag: "span",
                    classes: "fittable-box details-fonts-program-details-header",
                    content: "не указано"
                }
            ]
        }
    ]
});


/*
 * Descripting text of the program with scrolling
 */
enyo.kind({
    name: DetailsDescriptionScroller,
    kind: "moon.Scroller", 
    layoutKind: "FittableRowsLayout",
    classes: "fittable-box details-block-distance",
    fit: true, 
    paginationPageMultiplier: 0.2,
    spotlightPagingControls: true, // bugfix: required for spotlight on arrow of the scroll
    horizontal: "hidden", 
    
    components: [
        {
            kind: "moon.BodyText",
            name: "description", 
            classes: "fittable-box details-fonts-program-details-content",
            content: "описание отсутствует"
        }
    ]
});



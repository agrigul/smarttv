enyo.kind({
    name: "ProgramWatcherView",
    kind: "enyo.Control",
    fit: true,
    create: enyo.inherit(function (sup) {
        return function () {
            sup.apply(this, arguments);
            var programModel = this.programModel || (new ProgramDetailsModel());
            this.set("programModel", programModel);
        };
    }),
    
    layoutKind: "FittableRowsLayout",
    classes: "programWatcherView",
    published: {
        isSlideoutOpened: false
    },
    
    
    bindings: [
        { from: ".model.stream_url", to: ".$.videoPlayer.src" },
        { from: ".programModel.title", to: ".$.programTitle.content" },
        { from: ".programModel.image_url", to: ".$.poster.src" },
        { from: ".programModel.film_type", to: ".$.programTypeName.content" },
        { from: ".programModel.start_time", to: ".$.programProgress.startTime" },
        { from: ".programModel.end_time", to: ".$.programProgress.endTime" },
        { from: ".programModel.description", to: ".$.programDescription.content" }
    ],
    
    observers: {
        isSlideoutOpenedChanged: ["isSlideoutOpened"]
    },
    
    
    isSlideoutOpenedChanged: function () {
        if (this.get("isSlideoutOpened")) {
            this.loadProgramInfo();
        } else {
            this.$.slideout.set("isOpened", false);
        }
        
        if (!this.get("isSlideoutOpened")) return;
        this.$.slideout.resized();
    },
    
    loadProgramInfo: function () {
        var currentProgram = this.model.getCurrentProgram();
        if (!currentProgram) return;
        this.programModel.set("id", currentProgram.get("id"));
        this.programModel.fetch({ success: enyo.bind(this, this.programInfoLoded) });
    },
    
    programInfoLoded: function () {
        if (!this.get("isSlideoutOpened")) return;
        enyo.Spotlight.spot(this.$.descriptionScroller);
        this.$.slideout.resized();
        this.$.slideout.set("isOpened", true);
    },
    
    openPrograms: function () {
        app.OpenPage(pages.Programs);
    },
    
    
    components: [
        {
            name: "videoPlayer",
            kind: "enyo.Video",
            classes: "backgroundVideo",
            autoplay: true,
            src: ""
        },
        {
            fit: true,
            layoutKind: "FittableRowsLayout",
            classes: "slideOutContainer",
            components: [
                {
                    name: "slideout",
                    classes: "slideOut__program-info",
                    fit: true,
                    published: {
                        isOpened: false
                    },
                    isOpenedChanged: function () {
                        this.addRemoveClass("open", this.get("isOpened"));
                    },
                    layoutKind: "FittableRowsLayout",
                    components: [
                        {
                            tag: "div",
                            classes: "posterContainer",
                            components: [
                                {
                                    kind: "enyo.Image",
                                    name: "poster",
                                    src: "",
                                    alt: "poster"
                                }
                            ]
                        },
                        {
                            tag: "h1",
                            name: "programTitle",
                            content: "",
                            classes: "programTitle"
                        },
                        {
                            kind: "moon.Divider",
                            classes: "hr-divider"
                        },
                        {
                            classes: "programMeta",
                            components: [
                                {
                                    name: "programTypeName",
                                    classes: "programType",
                                    content: "не указан"
                                },
                                {
                                    classes: "progressContainer",
                                    components: [
                                        {
                                            kind: "ProgramProgress",
                                            name: "programProgress"
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            name: "descriptionScroller",
                            kind: "moon.Scroller",
                            fit: true,
                            spotlightPagingControls: true,
                            defaultSpotlightLeft: "homeFooterItem",
                            horizontal: "hidden",
                            components: [
                                {
                                    name: "programDescription",
                                    classes: "descriptionText",
                                    content: ""
                                }
                            ]
                        }
                    ]
                }
            ]
        },
        { kind: "ColoredButtonsNavigationMenuInWatchViewer" }
    ]
});


/*
 *  Footer with navigation buttons
*/
enyo.kind(
    {
        name: "ColoredButtonsNavigationMenuInWatchViewer",
        classes: "footerMenu",
        components: [
            {
                name: "RedButton",
                kind: "ProgrammableButton",
                buttonType: ProgrammableButtonType.Red,
                classes: "stackLeft",
                content: "Каналы",
                onPress: "OpenChannelsHandler"
            },
            {
                name: "GreenButton",
                kind: "ProgrammableButton",
                buttonType: ProgrammableButtonType.Green,
                classes: "stackLeft",
                content: "Передачи",
                onPress: "OpenProgramsHandler"
            },
            {
                name: "BlueButton",
                kind: "ProgrammableButton",
                buttonType: ProgrammableButtonType.Blue,
                classes: "stackRight",
                content: "Описание",
                onPress: "OpenDescriptionPressHandler"
            },
            { kind: "enyo.Signals", onkeydown: "ColoredButtonClickHandler" }
        ],
        
        ColoredButtonClickHandler : function (sender, event) {
            switch (event.keyCode) {
                case ProgrammableButtonType.Red.keyCode:
                    this.$.RedButton.KeyClick(sender, event);
                    break;
                case ProgrammableButtonType.Green.keyCode:
                    this.$.GreenButton.KeyClick(sender, event);
                    break;
                case ProgrammableButtonType.Blue.keyCode:
                    this.$.BlueButton.KeyClick(sender, event);
                    break;
                default: break;
            }
        },
        bindings: [
            { from: ".owner.model.channelId", to: ".channelId" }
        ],
        
        OpenChannelsHandler: function () {
            app.OpenPage(pages.Channels, { channelId : this.get("channelId") });
        },
        
        OpenProgramsHandler: function () {
            app.OpenPage(pages.Programs);
        },
        
        /*
         *  Shows a popup windows with current program details
        */
        OpenDescriptionPressHandler: function () {
            var parent = this.owner;
            var isSlideoutOpened = !parent.get("isSlideoutOpened"),
                spotlightAction = isSlideoutOpened ? "spot" : "unspot";
            parent.set("isSlideoutOpened", isSlideoutOpened);
            this.$.BlueButton.set("isActive", isSlideoutOpened);
        }
    });
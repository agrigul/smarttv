﻿﻿/*
 * Main entry point class for page
 */
enyo.kind({
    name: "BaseLayoutView",
    layoutKind: "FittableRowsLayout",
    
    components: [
        {
            name: "renderSection", 
            layoutKind: "FittableRowsLayout",
            fit: true
        }
    ],
    

    /*
     * Updates render section of the existing view with the new one (replaces div by new view and content)
     */
	UpdateViewByType : function (viewName) {
        this.$.renderSection.destroyClientControls();
        this.$.renderSection.createComponent({ kind: viewName });
    },
    

    /*
     * Update  render section  by view with model
     */
    UpdateViewByTypeAndModel : function (viewName, model) {
       
        this.$.renderSection.destroyClientControls();
        this.$.renderSection.createComponent({ name: viewName, kind: viewName, model: model }); // fix: fill the Name property to avoid names of id like "nameOfView2"
    },
    

    /*
     *  Dynamic update of render section
     */
    UpdateViewWithInstance : function (viewInstance, model) {
        throw Error("UpdateViewWithInstance is not implemented");
    }
});
/**
	Define and instantiate your enyo.Application kind in this file.  Note,
	application rendering should be deferred until DOM is ready by wrapping
	it in a call to enyo.ready().
*/

/*
 *  Application class. Entry point.
*/
enyo.kind({
    name: "cdnvideo.Application",
    kind: "enyo.Application",
    view: "BaseLayoutView",
    classes: "enyo-fit",
    _router : new RouterService(),
    
    create: function () {
        this.inherited(arguments);
        window.addEventListener('popstate', enyo.bindSafely(this, this._BackToPageFromHistory));
        this.initKeyHooks();
    },
    
    //* @public

    components: [
        { name: "detailsController", kind: "DetailsController" },
        { name: "programsController", kind: "ProgramsController" },
        { name: "channelsController", kind: "ChannelsController" },
        { name: "programWatcherController", kind: "ProgramWatcherController" }
    ],
    
    
    OpenStartPage: function () {
        this.$.programWatcherController.Index();
    },
    

    /*
     * Navigates to the page from parameter. Parameter is a value from Pages enum(collection)
    */
    OpenPage: function (page) {
        this._router.RouteTo(page, arguments, this);
    },

    
    /*
     *  Create new view in baselayout, when user changes the page. 
     *  Don't call this method from page, use OpenPage instead.
     *  This method is just for controllers to specify which view should be set in renderSection of Baselayout
    */
    UpdateView : function (view, model) {
        
        if (typeof view === "string" && !model) {
            this.view.UpdateViewByType(view);
        } 
        else if (typeof view === "string" && model) {
            this.view.UpdateViewByTypeAndModel(view, model);
        }
        else if (typeof view === "object" && view) {
            this.view.UpdateViewWithInstance(view, model);
        } 
        else {
            throw Error("Incorrect parameters to update view: view is " + view);
        }
        
        this.view.render();
    },

    /**
     * @private
     */
    initKeyHooks: function(){
       var keyHooks = this.createComponent({
           name: "keyHooks",
           kind: "KeyHookContainer",
           defaultHookAction: enyo.bind(this, function(hook){
               var channelId = hook.model.get("channelId");
               if(channelId != undefined){
                   this.OpenPage(pages.ProgramWatcher, { channelId: channelId});
               }
           })
       });
    },

    /**
     * @private
     */
    getHooks: function(){
        return this.$.keyHooks.getHooks();
    },

    /**
     * @private
     */
    disableDefaultHooks: function(){
        this.$.keyHooks.disableDefaultAction();
    },

    /**
     * @private
     */
    enableDefaultHooks: function(){
        this.$.keyHooks.enableDefaultAction();
    },
    
    //* @private

    /*
     * Gets the state of the previous page and move to that page using controller and action from history
    */
    _BackToPageFromHistory : function (historyEvent) {
        if (!historyEvent)
            throw Error("historyEvent can't be null");
        
        var previousPage = historyEvent.state;
        if (previousPage) {
            app.$[previousPage.controller][previousPage.action](previousPage.params);
        } else {
            this.OpenStartPage();
        }
    },
});



enyo.ready(function () {
    app = new cdnvideo.Application({ name: "CdnVideoApp" });

    //start application in 5-way mode
    enyo.Spotlight.setPointerMode(false);
    app.OpenStartPage();
});


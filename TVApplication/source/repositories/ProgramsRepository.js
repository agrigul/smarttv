﻿
/*
 *  Connection to data source
 */
enyo.kind({
    name: "cdntvServer.Source",
    kind: "enyo.JsonpSource",
    urlRoot: window.config.serverRootUrl(),
    
    fetch: function (rec, opts) {
        opts.params = enyo.clone(rec.params);
        opts.params.format = "json";
        this.inherited(arguments);
    },
});


/*
 *  Store for recrods from server (singletone implementation)
 */
enyo.store.addSources({ cdntvServer: "cdntvServer.Source" });



/*
 *  Repository for programs
 */
enyo.kind({
    name: "ProgramsRepository",
    statics: {
        programDetials: null,  
        favoritePrograms: null,
    },
    
    _programDetials : null,
    _favoritePrograms : null,
    _fetchOptions : { strategy: "merge" },
    
    create: function () {
        this.inherited(arguments);
        
        this.InitSingletoneModels();
    },

    /*
     * Initialisation of models and collections to be used to store records  in enyo.Store.
     * Collections with items from server  have to be singltons, otherwise there are duplicating in Store and grows fast
     */
    InitSingletoneModels : function() {
        
        if (!ProgramsRepository.programDetials) {
            ProgramsRepository.programDetials = new ProgramDetailsModel();
        }
        
        if (!ProgramsRepository.favoritePrograms) {
            ProgramsRepository.favoritePrograms = new FavoriteProgramsCollection();
        }
        
        this._programDetials = ProgramsRepository.programDetials;
        this._favoritePrograms = ProgramsRepository.favoritePrograms;  
    },
    
    
    /*
     *  Returns a collection of favorite programs (FavoriteProgramsModel)
     */
    GetFavoritePrograms: function () {
        
        this._favoritePrograms.fetch(this._fetchOptions);
        return this._favoritePrograms;
    },
    

    /*
     *  Returns information about program (ProgramDetailsModel)
     */
    GetProgramDetailsById: function (id) {
        
        this._programDetials.set("id", id);
        this._programDetials.fetch(this._fetchOptions);
        return this._programDetials;
    }
});
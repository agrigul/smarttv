// Store implemntation with disabled models and collections caching
// we removed cause now it is yet not properly implemented by enyo developers. Now if you call fetch collection that
// was once fetched it tries to create fetched models, but models with same ids exist in cache and store throws
// exception. In CDNTV app we don`t need collection model caching, so this store implementation is only abstraction
// over Sources.

(function(enyo){

    enyo.kind({
        name: "CDNTVStore",
        kind: "enyo.Store",
        addRecord: function(){},
        addCollection: function(){},
        removeRecord: function(){},
        removeCollection: function(){},
        _recordKeyChanged: function(){}
    });

    enyo.store = new CDNTVStore();
})(enyo);

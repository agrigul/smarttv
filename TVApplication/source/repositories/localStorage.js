/**
 * current enyo boilerplate lacks local storage, so it is copied from enyo@2.4.0.
 * TODO after updating boilerplate to enyo@2.4.0 remove this source
 */
(function (enyo, scope) {

    var localStorage = scope.localStorage;

    if (localStorage) {
        var kind = enyo.kind
            , json = enyo.json
            , uuid = enyo.uuid;

        var Source = enyo.Source
            , Collection = enyo.Collection
            , Model = enyo.Model;

        var CODES = {
            UNIQUE_URL: 'A Collection must have a unique url property when using localStorage',
            UNIQUE_PRIMARY_KEY: 'A Model must have a unqiue primaryKey to be able to fetch or commit it to localStorage'
        };

        /**
         * A [localStorage]{@glossary localStorage} [source]{@link enyo.Source}. This
         * [kind]{@glossary kind} is only an available on platforms that support the
         * localStorage Web standard.
         *
         * It is important to note that usage of this source requires that
         * [models]{@link enyo.Model} and [collections]{@link enyo.Collection} use
         * their respective `url` properties ({@link enyo.Model.url} and
         * {@link enyo.Collection.url}). Any collection that needs to be
         * [committed]{@link enyo.Collection#commit} must have a unique `url` value.
         * Any model that will be [committed]{@link enyo.Model#commit} directly, or
         * within an {@link enyo.Collection}, must have a unique
         * [primaryKey]{@link enyo.Model#primaryKey}.
         *
         * @class enyo.LocalStorageSource
         * @extends enyo.Source
         * @public
         */
        kind( {

            /**
             * @private
             */
            name: 'enyo.CustomLocalStorageSource',

            /**
             * @private
             */
            kind: Source,

            /**
             * @private
             */
            noDefer: true,

            /**
             * The namespace of all [models]{@link enyo.Model} and
             * [collections]{@link enyo.Collection} that will be stored by this
             * {@link enyo.LocalStorageSource}.
             *
             * @type {String}
             * @public
             */
            prefix: "CDNTV-App",

            /**
             * Implementation of {@link enyo.Source.fetch}.
             *
             * @see enyo.Source.fetch
             * @public
             */
            fetch: function (model, opts) {
                var id, res;
                id = this.getModelId(model);
                res = this.get(id);
                opts.success(res);
            },

            /**
             * Implementation of {@link enyo.Source.commit}.
             *
             * @see enyo.Source.commit
             * @public
             */
            commit: function (model, opts) {
                var id, data;
                id = this.getModelId(model);
                data = model.raw();
                this.set(id, data);
                opts.success();
            },

            /**
             * Implementation of {@link enyo.Source.destroy}.
             *
             * @see enyo.Source.destroy
             * @public
             */
            destroy: function (model, opts) {
                var modelId = this.getModelId(model),
                    storageId = this.storageId(modelId);
                localStorage.removeItem(storageId);
                opts.success();
            },

            /**
             * Not implemented.
             *
             * @public
             */
            find: function (ctor, opts) {
                throw new Error("method find is not implemented on localStorage source");
            },

            getModelId: function(model){
                var id;
                if (model instanceof Collection) {
                    if (!(id = model.url)) throw new Error(CODES.UNIQUE_URL);
                }
                else if (model instanceof Model) {
                    if (!(id = model.attributes[model.primaryKey])) throw new Error(CODES.UNIQUE_PRIMARY_KEY);
                }
                return id;
            },

            /**
             * @private
             */
            storageId: function(key){
                return this.prefix + ":" + key;
            },

            /**
             * @private
             */
            get: function(key){
                var result = localStorage.getItem(this.storageId(key));
                return json.parse(result);
            },

            /**
             * @private
             */
            set: function(key, item){
                localStorage.setItem(this.storageId(key), json.stringify(item))
            }
        });

        enyo.store.addSources({ customLocalStorage: "enyo.CustomLocalStorageSource"})

    }

})(enyo, this);
.".\package-vars"

#clear and create package folder 
echo "clear package folder"
rm $PACKAGE_FOLDER_PATH -Recurse -Force
md $PACKAGE_FOLDER_PATH

#package application
echo "packaging started"
$EXCLUDES="-e " + [string]::join(" -e ", $EXCLUDE_FROM_PACKAGE.split(" "))
& $env:LG_WEBOS_TV_SDK_HOME\CLI\ares-package.cmd --outdir $PACKAGE_FOLDER_PATH $EXCLUDES --no-minify .\
echo "packaging finished"
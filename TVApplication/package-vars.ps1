$PACKAGE_FOLDER_NAME="Packaged"
$PACKAGE_FOLDER_PATH=".\$PACKAGE_FOLDER_NAME"
$EXCLUDE_FROM_PACKAGE=".git .idea .gitignore .gitmodules .project .settings .npmignore debug.html npm-debug.log README.md web.config Packaged package.bat package-nominify.bat"
$EMULATOR_NAME="emulator"
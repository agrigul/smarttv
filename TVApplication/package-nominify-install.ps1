
.".\package-vars.ps1"
.".\package-nominify.ps1"

#install app on emulator

$APPLICATION_NAME=@(gci "$PACKAGE_FOLDER_PATH\*.ipk")[0].Name
$APPLICATION_PATH="$PACKAGE_FOLDER_PATH\$APPLICATION_NAME"

echo "app installing"
& $env:LG_WEBOS_TV_SDK_HOME\CLI\ares-install.cmd --device $EMULATOR_NAME $APPLICATION_PATH
echo "app installed"

#launch app on emulator

$APPLICATION_ID=& node package_tools\getAppId.js appinfo.json

echo "starting app"
& $env:LG_WEBOS_TV_SDK_HOME\CLI\ares-launch.cmd --device $EMULATOR_NAME $APPLICATION_ID
echo "app started"
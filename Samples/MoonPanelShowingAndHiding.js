// -------- detailsPanelView.js

enyo.kind({
		name: "detailsPanel",
       	kind: "moon.Panel", 
       	title: "Details",	           			            	  
       	components: [ 
       	              {
       	            	  kind: "moon.Button",
       	            	  name: "prevBtn",
       	            	  content: "Previous Next",
       	            	  }
       	],
       	events: {
       		onRequestPopPanel: ""
       	},
       	handlers: {ontap: "buttonTapped"},
       	
       	buttonTapped: function(inSender, inEvent) {
       		this.doRequestPopPanel({panel: {kind: "detailsPanel", model: inEvent.model}});
       	}
});

enyo.kind({
	name : "BaseView",
	classes: "moon enyo-fit",
	events: {
           onRequestPushPanel: "",
           onRequestPopPanel: ""
    },
	components:		[


 	 
	           		 {name: "panels", 
	           			 classes: "enyo-fit", 
	           			 pattern: "none", 
	           			 arrangerKind: "CarouselArranger",
	           			 kind: "moon.Panels",
	           			 components: [
	           			              {
	           			            	name: "mainPanel",
	           			            	kind: "moon.Panel", 
	           			            	title: "Main",	           			            	
	           			            	components: [ 
	           			            	              {
	           			            	            	  kind: "moon.Button",
	           			            	            	  name: "nextBtn",
	           			            	            	  content: "Show Next",	           			            	            	  
	           			            	            	  }
	           			            	],
	           			            	events: {
	           			            		onRequestPushPanel: ""
	           			            	},
	           			            	handlers: {ontap: "buttonTapped"},
	           			            	buttonTapped: function(inSender, inEvent) {
	           			            		this.doRequestPushPanel({panel: {kind: "detailsPanel", model: inEvent.model}});
	           			            	}
	           			              },           			               
	           			 ]}
		],
	handlers: {
	        onRequestPushPanel: "pushPanel",
	        onRequestPopPanel: "popPanel"
	},
	
	pushPanel: function(inSender, inEvent) {
	    this.$.panels.pushPanel(inEvent.panel);
	},
	
	popPanel: function(inSender, inEvent) {		
	    this.$.panels.previous();
	    this.$.panels.popPanels(this.$.panels.getActive()-1);	    
	}	
	
});






// ----- app.js    


enyo.kind({
    name: "my.Application",
    kind: "enyo.Application",
    view: "BaseView", // default start view    
   
});

enyo.ready(function () {
    app = new my.Application({name: "app"});
});
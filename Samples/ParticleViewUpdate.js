//------  controller.js + IndexView.js + editview.JS


enyo.kind({
    name : "BaseView",
    kind: "enyo.View",
    components: [
                 {content: "HelloWorld,  This is Header"},
                 {name: "renderSection", kind: "IndexView"},                 
                 ], 
});

enyo.kind({
    name : "IndexView",
    kind: "enyo.View",
    components: [
                 {content: "HelloWorld,  This is Index"},
                 {content: "This is the Index view"},
                 {kind: "moon.ToggleButton", content: "Show Edit", ontap: "buttonTapped"}
                 ],
    // redirect to Edit View
    buttonTapped: function(sender, event){
        app.controllers.homeController.Edit("message(model) to Edit View");
    }

});


enyo.kind({
    name : "EditView",
    kind: "enyo.View",
    message: "no msg",
    components: [
                 {name: "headWithId", content: "Hello! This is EDIT."},
                 {content: "This is the Edit view"},
                 {kind: "moon.ToggleButton", content: "Show Index", ontap: "buttonTapped"}
    ],

    bindings: [
              {from: ".message", to:".$.headWithId.content"}
    ],

    // redirect to Index View
    buttonTapped: function(sender, event){              
        app.controllers.homeController.Index();     
    }
});


enyo.kind({
    name : "HomeController",
    kind: "enyo.Controller",

    Index : function(){     
    	app.UpdateRenderSection(new  IndexView()); 
        //app.set("view", new  IndexView()); // this code updates the view of app, but maybe there is a better way to do it?      
    },

    Edit : function(msg){
    	app.UpdateRenderSection(new  EditView({message: msg}));
    }
});





// ----- app.js    
enyo.kind({
    name: "my.Application",
    kind: "enyo.Application",
    view: "BaseView", // default start view    
    controllers: [ // I use 2.3 version and know about this rudiment property.
                {name: "homeController", kind: "HomeController"}                
            ],          
    viewChanged : function() { 
        this.inherited(arguments);        
        app.render();  // this updates the view. Changes from index to edit and back. 
    },
	
	UpdateRenderSection: function(view){
		debugger;
		view.renderInto(this.view.$.renderSection.id);
	}
});

enyo.ready(function () {
    app = new my.Application({name: "app"});
});
﻿
using System;
using System.Web.Http;
using Newtonsoft.Json.Linq;

namespace WebServerForTests.Controllers
{
    public class CategoriesController : ApiController
    {
        // GET api/categories/
        public long ConvertToTimestamp(DateTime value)
        {
            DateTime Epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            TimeSpan elapsedTime = value - Epoch;
            return (long)elapsedTime.TotalSeconds;
        }

        public JToken Get()
        {
            var startTime = ConvertToTimestamp(DateTime.Now);
            var endTime = ConvertToTimestamp(DateTime.Now.AddMinutes(4));


            JToken json = JObject.Parse(@"
                    {
                        'data': {
                            'suited_programs': [ // список передач из всех категорий, наиболее подходящих под интересы пользователя
                                {
                                    'id': '1',
                                    'channel_name' : 'Первый канал',
                                    'title': 'Кто хочет стать миллионером',
                                    'image_url': 'http://mars.jpl.nasa.gov/msl-raw-images/proj/msl/redops/ods/surface/sol/00399/opgs/edr/ncam/NRB_432926428EDR_T0160148NCAM00207M_-thm.jpg',
                                    'start_time': '" + startTime + @"',
                                    'end_time': '" + endTime + @"'
                                },
                                {
                                    'id': '2',
                                    'channel_name' : 'РТР планета',
                                    'title': 'Новости',
                                    'image_url': 'http://www.discoverychannel.ru/wp-content/uploads/2014/02/31841_EP106_006.jpg',
                                    'start_time': '" + startTime + @"',
                                    'end_time': '" + endTime + @"'
                                },
                                {
                                    'id': '3',
                                    'channel_name' : 'Discovery',
                                    'title': 'Будущее с джеймсом Вудсом: как стать суперчеловеком? 3',
                                    'image_url': '/assets/programPoster.jpg',
                                    'start_time': '" + startTime + @"',
                                    'end_time': '" + endTime + @"'
                                },
                                {
                                    'id': '4',
                                    'channel_name' : 'РТР планета',
                                    'title': 'Будущее с джеймсом Вудсом: как стать суперчеловеком? 4',
                                    'image_url': 'http://www.hdwallpapersimages.com/wp-content/uploads/images/Frozen-Logo-Symbol-HD-Images.jpg',
                                    'start_time': '" + startTime + @"',
                                    'end_time': '" + endTime + @"'
                                },                                {
                                    'id': '5',
                                    'channel_name' : 'РТР планета',
                                    'title': 'Будущее с джеймсом Вудсом: как стать суперчеловеком? 5',
                                    'image_url': 'http://wpmedia.o.canada.com/2014/04/528968726.jpg',
                                    'start_time': '" + startTime + @"',
                                    'end_time': '" + endTime + @"'
                                },                                {
                                    'id': '6',
                                    'channel_name' : 'РТР планета',
                                    'title': 'Будущее с джеймсом Вудсом: как стать суперчеловеком? 6',
                                    'image_url': 'https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcS1VqyuigsBVM_FVA_MJdXBZScC5EKhmXKe0GJJIHGHceZaOvye',
                                    'start_time': '" + startTime + @"',
                                    'end_time': '" + endTime + @"'
                                },                                {
                                    'id': '7',
                                    'channel_name' : 'РТР планета',
                                    'title': 'Будущее с джеймсом Вудсом: как стать суперчеловеком? 7',
                                    'image_url': 'assets/programPoster.jpg',
                                    'start_time': '" + startTime + @"',
                                    'end_time': '" + endTime + @"'
                                },

                            ]
                        }
                    }

            ");
            return json;
        }
    }
}

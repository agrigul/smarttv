﻿using System.Web.Http;
using Newtonsoft.Json.Linq;

namespace WebServerForTests.Controllers
{
    public class ChannelsController : ApiController
    {
        // GET api/channels/

        public JToken Get()
        {
            JToken json = JObject.Parse(@"
                    {
                    'data': {
                        'all_channels': [
                            {
                                'id': 1,
                                'title': 'Первый',
                                'logo_url': 'assets/channels/1.jpg',
                                'stream_url': 'http://clips.vorwaerts-gmbh.de/VfE_html5.mp4'
                            },
                            {
                                'id': 2,
                                'title': 'РТР Планета',
                                'logo_url': 'assets/channels/2.jpg',
                                'stream_url': 'http://images.all-free-download.com/footage_preview/webm/clouds_timelapse_74.webm'
                            },
                            {
                                'id': 3,
                                'title': 'НТВ Плюс',
                                'logo_url': 'assets/channels/3.png',
                                'stream_url': 'http://clips.vorwaerts-gmbh.de/VfE_html5.mp4'
                            },
                            {
                                'id': 4,
                                'title': 'CNN international',
                                'logo_url': 'assets/channels/4.png',
                                'stream_url': 'http://images.all-free-download.com/footage_preview/webm/clouds_timelapse_74.webm'
                            },
                            {
                                'id': 5,
                                'title': 'ТВ центр',
                                'logo_url': 'assets/channels/5.png',
                                'stream_url': 'http://clips.vorwaerts-gmbh.de/VfE_html5.mp4'
                            },
                            {
                                'id': 6,
                                'title': 'РТР Планета',
                                'logo_url': 'assets/channels/6.png',
                                'stream_url': 'http://images.all-free-download.com/footage_preview/webm/clouds_timelapse_74.webm'
                            },
                            {
                                'id': 7,
                                'title': 'CNN international',
                                'logo_url': 'assets/channels/4.png',
                                'stream_url': 'http://clips.vorwaerts-gmbh.de/VfE_html5.mp4'
                            },
                            {
                                'id': 8,
                                'title': 'ТВ центр',
                                'logo_url': 'assets/channels/5.png',
                                'stream_url': 'http://images.all-free-download.com/footage_preview/webm/clouds_timelapse_74.webm'
                            },
                            {
                                'id': 9,
                                'title': 'РТР Планета',
                                'logo_url': 'assets/channels/6.png',
                                'stream_url': 'http://clips.vorwaerts-gmbh.de/VfE_html5.mp4'
                            }
                        ]
                        }
                    }
            ");
            return json;
        }
    }
}

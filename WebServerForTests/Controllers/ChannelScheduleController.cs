﻿using System;
using System.Web.Http;
using Newtonsoft.Json.Linq;

namespace WebServerForTests.Controllers
{
    public class ChannelScheduleController : ApiController
    {
        private readonly DateTime Epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        private string description =
          @"Представьте, что вы можете читать мысли других людей с помощью своего мобильного телефона, а вместо автомобиля используете костюм из фильма «Железный человек». Вы не страдаете от болезней, потому что даже рак исцеляется с помощью специальных имплантатов. Технологии, которые несколько дет назад встречались только в фантастической литературе, постепенно проникают в повседневную жизнь людей, и будущее, существовавшее только в воображении, начинает обретать форму.В новой программе Discovery Channel «Будущее с Джеймсом Вудсом» знаменитый актер изучает последние технологические прорывы и их возможные последствия для будущего человечества. Каждая передача посвящена одной потрясающей идее или открытию, способному кардинально изменить привычную нам жизнь. Что происходит в ключевых областях науки? Чем это обернется для нас? Джеймс задаст важные вопросы, начнет дискуссию и при помощи уникальных технологий съемки и компьютерного моделирования покажет потрясающие картины нашего будущего.";

        public long ConvertToTimestamp(DateTime value)
        {
            TimeSpan elapsedTime = value - Epoch;
            return (long) elapsedTime.TotalSeconds;
        }

        
        // GET api/channelschedule/

        public JToken Get()
        {
            var schedule = new JArray();
            var currentTime = DateTime.UtcNow;

            for(int i = -5; i < 10; i ++){
                schedule.Add(new JObject(
                        new JProperty("id", (Guid.NewGuid()).ToString()),
                        new JProperty("title", "Будущее с джеймсом Вудсом: как стать суперчеловеком?"),
                        new JProperty("image_url", "http://www.discoverychannel.ru/wp-content/uploads/2014/02/31841_EP106_006.jpg"),
                        new JProperty("start_time", ConvertToTimestamp(currentTime.AddMinutes(i))),
                        new JProperty("end_time", ConvertToTimestamp(currentTime.AddMinutes(i + 1).AddSeconds(-1))),
                        new JProperty("short_description", description)
                    ));
            }
            


            JToken json =  new JObject(
                new JProperty("data", new JObject(
                        new JProperty("channel", new JObject(
                                new JProperty("id", 2),
                                new JProperty("title", "РТР планета"),
                                new JProperty("logo_url", "assets/channels/1.jpg"),
                                new JProperty("stream_url", "http://clips.vorwaerts-gmbh.de/VfE_html5.mp4")
                            )),
                        new JProperty("schedule", schedule)
                    ))
                );
            return json;
        }
    }
}

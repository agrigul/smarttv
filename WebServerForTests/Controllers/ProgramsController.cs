﻿using System;
using System.Web.Http;
using Newtonsoft.Json.Linq;

namespace WebServerForTests.Controllers
{
    public class ProgramsController : ApiController
    {
        private readonly DateTime Epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        private long ConvertToTimestamp(DateTime value)
        {
            TimeSpan elapsedTime = value - Epoch;
            return (long)elapsedTime.TotalSeconds;
        }
        // GET api/porgrams
        public JToken Get()
        {
            return null;
        }

        // GET api/porgrams/5
        
       public JToken Get(int id)
        {
            JToken json = JObject.Parse(@"
                {
                   'data': {
                       'id': '12345',
                       'title': 'Будущее с Джеймсом Вудсом: как стать суперчеловеком?',
                       'original_title': 'Futureescape with James Woods (нет в API)',
                       'year' : '2014',
                       'rate': '12',
                       'logo_channel_url': 'assets/details_channel_logo.png',
                       'image_url': 'http://www.discoverychannel.ru/wp-content/uploads/2014/02/31841_EP106_006.jpg',
                       'stream_url': 'http://clips.vorwaerts-gmbh.de/VfE_html5.mp4',
                       'start_time': '" + ConvertToTimestamp(DateTime.UtcNow) + @"',
                       'end_time': '" + ConvertToTimestamp(DateTime.UtcNow.AddMinutes(1)) + @"',
                       'film_type': 'Документальный фильм (нет в API)',
                       'logo_film_type_url' : 'assets/icon-movie-tape.png',
                       'channel_name' : 'РТР планета (нет в API)',
                       'short_description': 'короткое описание передачи',
                       'genre': 'научно-популярный, познавательный, фантастический(нет в API. это теги?)',
                       'description': '" + description + description + description + @"'

                   }
                }
            ");
            return json;
        }


        private string description =
            @"Представьте, что вы можете читать мысли других людей с помощью своего мобильного телефона, а вместо автомобиля используете костюм из фильма «Железный человек». Вы не страдаете от болезней, потому что даже рак исцеляется с помощью специальных имплантатов. Технологии, которые несколько дет назад встречались только в фантастической литературе, постепенно проникают в повседневную жизнь людей, и будущее, существовавшее только в воображении, начинает обретать форму.В новой программе Discovery Channel «Будущее с Джеймсом Вудсом» знаменитый актер изучает последние технологические прорывы и их возможные последствия для будущего человечества. Каждая передача посвящена одной потрясающей идее или открытию, способному кардинально изменить привычную нам жизнь. Что происходит в ключевых областях науки? Чем это обернется для нас? Джеймс задаст важные вопросы, начнет дискуссию и при помощи уникальных технологий съемки и компьютерного моделирования покажет потрясающие картины нашего будущего.";

        //// POST api/porgrams
        //public void Post([FromBody]string value)
        //{
        //}

        //// PUT api/porgrams/5
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE api/porgrams/5
        //public void Delete(int id)
        //{
        //}
    }
}

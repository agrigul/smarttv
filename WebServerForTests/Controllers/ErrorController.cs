﻿using System.Web.Http;
using Newtonsoft.Json.Linq;

namespace WebServerForTests.Controllers
{
    public class ErrorController : ApiController
    {
        public JToken Get()
        {
            JToken json = JObject.Parse(@"
                    {
                       'error': {
                           'message': 'сообщение об ошибке',
                           'code': '404',
                       }
                    }
            ");
            return json;
        }
    }
}

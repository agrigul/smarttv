﻿using System.Web.Mvc;

namespace WebServerForTests.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}
